import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLReader {
	
	File xmlToRead;
	String xmlName;
	public XMLReader(String fileName) {
		xmlToRead=new File(fileName);
		xmlName=fileName;
	}
	public String readAllDatabases() throws IOException {
		String giveBack="";
		String read;
		BufferedReader input=new BufferedReader(new InputStreamReader(new FileInputStream(xmlToRead),"UTF-8"));
		while((read=input.readLine())!=null) {
			if(read.contains("<DataBase ")) {
				read=read.substring(read.indexOf("\"")+1);
				read=read.substring(0, read.length() - 2);
				giveBack+=read+"#";
			}
		}
		input.close();
		return giveBack;
	}
	public String readAllTablesOfDatabes(String databaseName) throws  SAXException, ParserConfigurationException, IOException {
		String giveBack="";
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
	    Document document = documentBuilder.parse(xmlToRead);
		
	    NodeList base=document.getElementsByTagName("DataBase");
	    NodeList tables=null;
	    for(int i=0;i<base.getLength();i++) {
	    	if(base.item(i).getNodeType()==Node.ELEMENT_NODE) {
	    		 Element data=(Element) base.item(i);
	    		 if(data.getAttribute("dataBaseName").equalsIgnoreCase(databaseName)) {
	    			 tables=data.getElementsByTagName("Table");
	    			 break;
	    		 }
	    	 } 
	    }
	    for(int i=0;i<tables.getLength();i++) {
	    	Element tmp=(Element) tables.item(i);
	    	giveBack+=tmp.getAttribute("tableName")+"#";
	    } 
	    
		return giveBack;
	}
	
	public int lastIndex() throws SAXException, IOException, ParserConfigurationException {
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
	    Document document = documentBuilder.parse(xmlToRead);
		
	    NodeList tables=document.getElementsByTagName("Table");
	    
	    boolean[] was=new boolean[1000];
	    for(int i=0;i<was.length;i++)
	    	was[i]=false;
	    
	    for(int i=0;i<tables.getLength();i++) {
	    	Element tmp=(Element) tables.item(i);
	    	int tmp1=Integer.parseInt(tmp.getAttribute("fileName"));
	    	was[tmp1]=true;
	    } 
	    NodeList indexes=document.getElementsByTagName("IndexFile");
	    for(int i=0;i<indexes.getLength();i++) {
	    	Element tmp=(Element) indexes.item(i);
	    		try {
	    			int tmp1=Integer.parseInt(tmp.getAttribute("fileName"));
	    			was[tmp1]=true;
	    		}catch(NumberFormatException v) {
	    	}
	    } 
	    NodeList foreign=document.getElementsByTagName("ForeignKey");
	    for(int i=0;i<foreign.getLength();i++) {
	    	Element tmp=(Element) foreign.item(i);
	    		try {
	    			int tmp1=Integer.parseInt(tmp.getAttribute("fileName"));
	    			was[tmp1]=true;
	    		}catch(NumberFormatException v) {
		    	
	    	}
	    } 
	    
	    for(int i=0;i<was.length-1;i++) {
	    	if(!was[i])
	    		return i;
	    }
	    return was.length;
	}
	
	public int tableIndex(String databaseName, String tableName) throws SAXException, IOException, ParserConfigurationException {
		int giveBack=0;
		
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
	    Document document = documentBuilder.parse(xmlToRead);
		
	    NodeList base=document.getElementsByTagName("DataBase");
	    NodeList tables=null;
	    for(int i=0;i<base.getLength();i++) {
	    	if(base.item(i).getNodeType()==Node.ELEMENT_NODE) {
	    		 Element data=(Element) base.item(i);
	    		 if(data.getAttribute("dataBaseName").equalsIgnoreCase(databaseName)) {
	    			 tables=data.getElementsByTagName("Table");
	    			 break;
	    		 }
	    	 } 
	    }
	    for(int i=0;i<tables.getLength();i++) {
	    	Element tmp=(Element) tables.item(i);
	    	if(tmp.getAttribute("tableName").equalsIgnoreCase(tableName)) {
	    		giveBack=Integer.parseInt(tmp.getAttribute("fileName"));
	    		break;
	    	}
	    } 
		
		return giveBack;
	}
	
	public int IndexOf(String databaseName, String tableName,String attribute) throws SAXException, IOException, ParserConfigurationException {
		int giveBack=0;
		//System.out.println(attribute);
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
	    Document document = documentBuilder.parse(xmlToRead);
		
	    NodeList base=document.getElementsByTagName("DataBase");
	    NodeList tables=null;
	    for(int i=0;i<base.getLength();i++) {
	    	if(base.item(i).getNodeType()==Node.ELEMENT_NODE) {
	    		 Element data=(Element) base.item(i);
	    		 if(data.getAttribute("dataBaseName").equalsIgnoreCase(databaseName)) {
	    			 tables=data.getElementsByTagName("Table");
	    			 break;
	    		 }
	    	 } 
	    }
	    NodeList indexes=null;
	    for(int i=0;i<tables.getLength();i++) {
	    	if(tables.item(i).getNodeType()==Node.ELEMENT_NODE) {
	    		 Element tmp=(Element) tables.item(i);
	    		 if(tmp.getAttribute("tableName").equalsIgnoreCase(tableName)) {
	    			 System.out.println(tableName);
	    			 indexes=tmp.getElementsByTagName("IndexFile");
	    			 break;
	    		 }
	    	 }
	    }
	    //System.out.println(indexes.item(0));
		if(indexes!=null) {
			for(int i=0;i<indexes.getLength();i++) {
				Element tmp=(Element) indexes.item(i);
				//System.out.println(tmp);
				if(tmp.getTextContent().equalsIgnoreCase(attribute)){
					giveBack=Integer.parseInt(tmp.getAttribute("fileName"));
					break;
				}
			}
		}
		return giveBack;
	}
	
	public int indexOfForeign(String databaseName, String tableName,String attribute) throws SAXException, IOException, ParserConfigurationException {
		int giveBack=0;
		
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
	    Document document = documentBuilder.parse(xmlToRead);
		
	    NodeList base=document.getElementsByTagName("DataBase");
	    NodeList tables=null;
	    for(int i=0;i<base.getLength();i++) {
	    	if(base.item(i).getNodeType()==Node.ELEMENT_NODE) {
	    		 Element data=(Element) base.item(i);
	    		 if(data.getAttribute("dataBaseName").equalsIgnoreCase(databaseName)) {
	    			 tables=data.getElementsByTagName("Table");
	    			 break;
	    		 }
	    	 } 
	    }
	    NodeList indexes=null;
	    for(int i=0;i<tables.getLength();i++) {
	    	Element tmp=(Element) tables.item(i);
	    	if(tmp.getAttribute("tableName").equalsIgnoreCase(tableName)) {
	    		indexes=tmp.getElementsByTagName("ForeignKey");
	    		break;
	    	}
	    } 
		if(indexes!=null) {
			for(int i=0;i<indexes.getLength();i++) {
				Element tmp=(Element) indexes.item(i);
				if(tmp.getTextContent().equalsIgnoreCase(attribute)){
					giveBack=Integer.parseInt(tmp.getAttribute("fileName"));
					break;
				}
			}
		}
		return giveBack;
	}
	
	public String readAllAtributeOfTable(String databaseName, String tableName) throws IOException, SAXException, ParserConfigurationException {
		String giveBack="";
		
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
	    Document document = documentBuilder.parse(xmlToRead);
		
	    NodeList base=document.getElementsByTagName("DataBase");
	    NodeList tables=null;
	    NodeList args=null;
	    for(int i=0;i<base.getLength();i++) {
	    	if(base.item(i).getNodeType()==Node.ELEMENT_NODE) {
	    		 Element data=(Element) base.item(i);
	    		 if(data.getAttribute("dataBaseName").equalsIgnoreCase(databaseName)) {
	    			 tables=data.getElementsByTagName("Table");
	    			 break;
	    		 }
	    	 } 
	    }
	    for(int i=0;i<tables.getLength();i++) {
	    	if(base.item(i).getNodeType()==Node.ELEMENT_NODE) {
		    	Element tmp=(Element) tables.item(i);
		    	if(tmp.getAttribute("tableName").equalsIgnoreCase(tableName)) {
		    		args=tmp.getElementsByTagName("Attribute");
		    	}
	    	}
	    } 
		
	    for(int i=0;i<args.getLength();i++) {
	    		Element tmp=(Element) args.item(i);
	    		if(tmp.getAttribute("type").equalsIgnoreCase("char"))
	    			giveBack+=tmp.getAttribute("attributeName")+"#"+tmp.getAttribute("type")+"#"+tmp.getAttribute("length")+"#";
	    		else
	    			giveBack+=tmp.getAttribute("attributeName")+"#"+tmp.getAttribute("type")+"#";
	    }
	    
		return giveBack;
	}
	
	public String readAllForeignKeysOfTable(String databaseName, String tableName) throws ParserConfigurationException, SAXException, IOException {
		String giveBack="";
		
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
	    Document document = documentBuilder.parse(xmlToRead);
		
	    NodeList base=document.getElementsByTagName("DataBase");
	    NodeList tables=null;
	    for(int i=0;i<base.getLength();i++) {
	    	if(base.item(i).getNodeType()==Node.ELEMENT_NODE) {
	    		 Element data=(Element) base.item(i);
	    		 if(data.getAttribute("dataBaseName").equalsIgnoreCase(databaseName)) {
	    			 tables=data.getElementsByTagName("Table");
	    			 break;
	    		 }
	    	 } 
	    }
	    Element foreign=null;
	    for(int i=0;i<tables.getLength();i++) {
	    	if(tables.item(i).getNodeType()==Node.ELEMENT_NODE) {
		    	Element tmp=(Element) tables.item(i);
		    	if(tmp.getAttribute("tableName").equalsIgnoreCase(tableName)) {
		    		NodeList foreigns=tmp.getElementsByTagName("ForeignKeys");
		    		foreign = (Element) foreigns.item(0);
		    		break;
		    	}
	    	}
	    }
	     
	    if(foreign!=null) {
		    NodeList keys=foreign.getElementsByTagName("ForeignKey");
		    for(int i=0;i<keys.getLength();i++) {
		    	if(keys.item(i).getNodeType()==Node.ELEMENT_NODE) {
		    		NodeList tmp=keys.item(i).getChildNodes();
		    		for(int k=0;k<tmp.getLength();k++) {
		    			if(tmp.item(k).getNodeName().equalsIgnoreCase("fkAttribute")) {
			    			System.out.println(tmp.item(k).getTextContent());
				    		giveBack+=tmp.item(k).getTextContent()+"#";  		
			    		}
		    		}
		    	}
		    }
	    }
		return giveBack;
	}
	
	public String readDestinationOfForeignKey(String databaseName, String tableName, String attribute) throws ParserConfigurationException, SAXException, IOException {
		String giveBack="";
		
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
	    Document document = documentBuilder.parse(xmlToRead);
		
	    NodeList base=document.getElementsByTagName("DataBase");
	    NodeList tables=null;
	    for(int i=0;i<base.getLength();i++) {
	    	if(base.item(i).getNodeType()==Node.ELEMENT_NODE) {
	    		 Element data=(Element) base.item(i);
	    		 if(data.getAttribute("dataBaseName").equalsIgnoreCase(databaseName)) {
	    			 tables=data.getElementsByTagName("Table");
	    			 break;
	    		 }
	    	 } 
	    }
	    Element foreign=null;
	    for(int i=0;i<tables.getLength();i++) {
	    	if(tables.item(i).getNodeType()==Node.ELEMENT_NODE) {
		    	Element tmp=(Element) tables.item(i);
		    	if(tmp.getAttribute("tableName").equalsIgnoreCase(tableName)) {
		    		NodeList foreigns=tmp.getElementsByTagName("ForeignKeys");
		    		foreign = (Element) foreigns.item(0);
		    		break;
		    	}
	    	}
	    }
	    try {
		    NodeList keys=foreign.getElementsByTagName("ForeignKey");
		    for(int i=0;i<keys.getLength();i++) {
		    	if(keys.item(i).getNodeType()==Node.ELEMENT_NODE) {
		    		Element tmp0=(Element) keys.item(i);
		    		NodeList kids=tmp0.getChildNodes();
		    		for(int j=0;j<kids.getLength();j++) {
		    			String tmp1=kids.item(j).getTextContent().replaceAll("\\s+","");
		    			if(tmp1.equalsIgnoreCase(attribute)) {
		    				j+=2;
		    				NodeList kids1=kids.item(j).getChildNodes();
		    				for(int k=1;k<kids1.getLength()-1;k++) {
		    					giveBack+=kids1.item(k).getTextContent().replaceAll("\\s+","")+"#";
		    					k++;
		    				}
		    				System.out.println(giveBack);
		    			}
		    		}
		    	}
		    }
	    }catch(NullPointerException e) {}
	    if(giveBack=="") return "1#";
		return giveBack;
	   }
	
	public String readAllAttributeWithUniqueOfTable(String databaseName, String tableName) throws ParserConfigurationException, SAXException, IOException {
		String giveBack="";
		
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
	    Document document = documentBuilder.parse(xmlToRead);
		
	    NodeList base=document.getElementsByTagName("DataBase");
	    NodeList tables=null;
	    for(int i=0;i<base.getLength();i++) {
	    	if(base.item(i).getNodeType()==Node.ELEMENT_NODE) {
	    		 Element data=(Element) base.item(i);
	    		 if(data.getAttribute("dataBaseName").equalsIgnoreCase(databaseName)) {
	    			 tables=data.getElementsByTagName("Table");
	    			 break;
	    		 }
	    	 } 
	    }
	    Element indexfile=null;
	    for(int i=0;i<tables.getLength();i++) {
	    	if(tables.item(i).getNodeType()==Node.ELEMENT_NODE) {
		    	Element tmp=(Element) tables.item(i);
		    	if(tmp.getAttribute("tableName").equalsIgnoreCase(tableName)) {
		    		NodeList indexfiles=tmp.getElementsByTagName("IndexFiles");
		    		indexfile = (Element) indexfiles.item(0);
		    		break;
		    	}
	    	}
	    }
	    if(indexfile!=null) {
		    NodeList keys=indexfile.getElementsByTagName("IndexFile");
		    for(int i=0;i<keys.getLength();i++) {
		    	if(keys.item(i).getNodeType()==Node.ELEMENT_NODE) {
		    		Element tmp=(Element) keys.item(i);
		    		if(tmp.getAttribute("isUnique").equalsIgnoreCase("1")) {
		    			String tmp1=tmp.getTextContent().replaceAll("\\s+","");
		    			giveBack+=tmp1+"#";
		    		}
		    	}
		    }
	    }
		return giveBack;
	}
	
	public String readAllAtributeOnlyOfTable(String databaseName, String tableName) throws ParserConfigurationException, SAXException, IOException {
		String giveBack="";
		
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
	    Document document = documentBuilder.parse(xmlToRead);
		
	    NodeList base=document.getElementsByTagName("DataBase");
	    NodeList tables=null;
	    NodeList args=null;
	    for(int i=0;i<base.getLength();i++) {
	    	if(base.item(i).getNodeType()==Node.ELEMENT_NODE) {
	    		 Element data=(Element) base.item(i);
	    		 if(data.getAttribute("dataBaseName").equalsIgnoreCase(databaseName)) {
	    			 tables=data.getElementsByTagName("Table");
	    			 break;
	    		 }
	    	 } 
	    }
	    for(int i=0;i<tables.getLength();i++) {
	    	if(base.item(i).getNodeType()==Node.ELEMENT_NODE) {
		    	Element tmp=(Element) tables.item(i);
		    	if(tmp.getAttribute("tableName").equalsIgnoreCase(tableName)) {
		    		args=tmp.getElementsByTagName("Attribute");
		    	}
	    	}
	    } 
	    
	    for(int i=0;i<args.getLength();i++) {
    		Element tmp=(Element) args.item(i);
    		giveBack+=tmp.getAttribute("attributeName")+"#";
    }
		
		return giveBack;
	}
	
	public String readAllAttributeWithIndexOfTable(String databaseName, String tableName) throws ParserConfigurationException, SAXException, IOException {
		String giveBack="";
		
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
	    Document document = documentBuilder.parse(xmlToRead);
		
	    NodeList base=document.getElementsByTagName("DataBase");
	    NodeList tables=null;
	    for(int i=0;i<base.getLength();i++) {
	    	if(base.item(i).getNodeType()==Node.ELEMENT_NODE) {
	    		 Element data=(Element) base.item(i);
	    		 if(data.getAttribute("dataBaseName").equalsIgnoreCase(databaseName)) {
	    			 tables=data.getElementsByTagName("Table");
	    			 break;
	    		 }
	    	 } 
	    }
	    Element indexfile=null;
	    for(int i=0;i<tables.getLength();i++) {
	    	if(tables.item(i).getNodeType()==Node.ELEMENT_NODE) {
		    	Element tmp=(Element) tables.item(i);
		    	if(tmp.getAttribute("tableName").equalsIgnoreCase(tableName)) {
		    		NodeList indexfiles=tmp.getElementsByTagName("IndexFiles");
		    		indexfile = (Element) indexfiles.item(0);
		    		break;
		    	}
	    	}
	    }
	    if(indexfile!=null) {
		    NodeList keys=indexfile.getElementsByTagName("IndexFile");
		    for(int i=0;i<keys.getLength();i++) {
		    	if(keys.item(i).getNodeType()==Node.ELEMENT_NODE) {
		    		Element tmp=(Element) keys.item(i);
		    		if(tmp.getAttribute("isUnique").equalsIgnoreCase("0")) {
		    			String tmp1=tmp.getTextContent().replaceAll("\\s+","");
		    			giveBack+=tmp1+"#";
		    		}
		    	}
		    }
	    }
		return giveBack;
	}
	
}
