import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;


public class Server {
	
	public static void main(String args[]) throws IOException, ParserConfigurationException, SAXException, TransformerException, InterruptedException {
		Connection server=new Connection(8087);
		String path="XML/scheme.xml";
		XMLReader read=new XMLReader(path);
		XMLDeleter delete=new XMLDeleter(path);
		XMLWriter write=new XMLWriter(path);
		RedisAcces redis=new RedisAcces(path);
		while(true) {
			server.acceptConnection();
			String received=server.receiveFromClient();
			//String received="9#Premium#Kecsitabla#4#egy#ketto#whitespace";
			if(received.charAt(0)=='0') { server.sendToClient("1");	}
			else { System.out.println(received); }
			if(received.charAt(0)=='1'  ) {
				switch( received.charAt(1)) {
					case '0':{
							String[] parts=received.split("#");
							redis.delete(parts[1], parts[2]);
							server.sendToClient("1");
							break;
						}
					case '1': {
							String[] parts=received.split("#");
							if(redis.deletePrimaryKey(parts[1], parts[2],parts[3])){
								server.sendToClient("1");
							}
							server.sendToClient("0");
							break;
						}
					case '2':{
							String[] parts=received.split("#");
							write.addIndexFile(parts[1], parts[2], parts[3]);
							redis.createIndex(parts[1], parts[2], parts[3]);
							server.sendToClient("1");
							break;
						}
					case '3':{
							String[] parts=received.split("#");
							write.addForeignKey(parts[1], parts[2], parts[3], parts[4], parts[5]);
							server.sendToClient("1");
							break;
					}
					case '4':{
							String[] parts=received.split("#");
							server.sendToClient(read.readAllAttributeWithUniqueOfTable(parts[1], parts[2]));
							break;
					}
					case '5':{
							String[] parts=received.split("#");
							server.sendToClient(read.readAllForeignKeysOfTable(parts[1], parts[2]));
							break;
					}
					case '6':{
						String[] parts=received.split("#");
						server.sendToClient(read.readDestinationOfForeignKey(parts[1], parts[2], parts[3]));
						break;
					}
					case '7':{
						String[] parts=received.split("#");
						write.addUnique(parts[1], parts[2], parts[3]);
						server.sendToClient("1");
						break;
					}
					case '8':{
						String[] parts=received.split("#");
						String value="";
						for(int i=3;i<parts.length-1;i++)
							value+=parts[i]+"#";
						server.sendToClient(redis.select(parts[1], parts[2],value));
						break;
					}
					case '9':{
						String[] parts=received.split("#");
						String where = "";
						String toJoin = "";
						int i = 2;
						while(!parts[++i].equals("w")) { toJoin += parts[i]+"#"; }
						if(parts[i+1].matches ("\\w+\\.?")) {
							for(int j=i+1;j<parts.length;j++) {
								where += parts[j]+"#";
							}
						}
						server.sendToClient(redis.join(parts[1],parts[2],toJoin, where));
						break;
					}
					default :{
						server.sendToClient(read.readAllDatabases());
					}
				}
			}
			if(received.charAt(0)=='2') {
				String[] parts=received.split("#");
				String toAdd=parts[1].replaceAll("\\s+","");
				server.sendToClient(read.readAllTablesOfDatabes(toAdd));
			}
			if(received.charAt(0)=='3') {
				String[] parts=received.split("#");
				server.sendToClient(read.readAllAtributeOfTable(parts[1],parts[2]));
			}
			if(received.charAt(0)=='4') {
				String[] parts=received.split("#");
				server.sendToClient(redis.select(parts[1], parts[2],null));
				//System.out.println(redis.select(parts[1], parts[2],null));
			}
			if(received.charAt(0)=='5') {
				String[] parts=received.split("#");
				if(read.readAllDatabases().contains(parts[1])) {
					delete.deleteDatabase(parts[1]);
					server.sendToClient("1");
				}
				else
					server.sendToClient("0");
			}
			if(received.charAt(0)=='6') {
				String[] parts=received.split("#");
				if(read.readAllDatabases().contains(parts[1])&&read.readAllTablesOfDatabes(parts[1]).contains(parts[2])) {
					delete.deleteTable(parts[1],parts[2]);
					server.sendToClient("1");
				}
				else
					server.sendToClient("0");
			}
			if(received.charAt(0)=='7') {
				String[] parts=received.split("#");
				if(!read.readAllDatabases().contains(parts[1])) {
					write.addDatabase(parts[1]);
					server.sendToClient("1");
				}
				else
					server.sendToClient("0");
			}
			if(received.charAt(0)=='8') {
				String[] parts=received.split("#");
				if(!(read.readAllDatabases().contains(parts[1])&&read.readAllTablesOfDatabes(parts[1]).contains(parts[2]))) {
					String[] attr=new String[250];
					for(int i=3;i<parts.length-1;i++) {
						attr[i-3]=parts[i];
					}
					write.addTable(parts[1], parts[2], attr);
					server.sendToClient("1");
				}
				else
					server.sendToClient("0");
			}
			if(received.charAt(0)=='9') {
				String[] parts=received.split("#");
				String value="";
				for(int i=4;i<parts.length-1;i++)
					value+=parts[i]+"#";
				if(redis.insert(parts[1], parts[2], parts[3], value))
					server.sendToClient("1");
				else 
					server.sendToClient("0");
			}
		}
	}
	
	
}
