import java.io.IOException;
import java.util.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import javax.xml.transform.dom.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.*;

public class XMLWriter {
	String xmlToRead;
	int index;
	public XMLWriter(String fileName) {
		xmlToRead=fileName;
		index=0;
	}
	public void addDatabase(String databaseName) throws ParserConfigurationException, SAXException, IOException, TransformerException {
		 DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
	     DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
	     Document document = documentBuilder.parse(xmlToRead);
	     Element root = document.getDocumentElement();
	     
	     Collection<Database> databases = new ArrayList<Database>();
	     databases.add(new Database());
	     
	     Element newDatabase = document.createElement("DataBase");
	     newDatabase.setAttribute("dataBaseName", databaseName);
	     newDatabase.setTextContent("\n");
	            
	     root.appendChild(newDatabase);
	     
	     DOMSource source = new DOMSource(document);
	     TransformerFactory transformerFactory = TransformerFactory.newInstance();
	     Transformer transformer = transformerFactory.newTransformer();
	     StreamResult result = new StreamResult(xmlToRead);
	     transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		 transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
	     transformer.transform(source, result);

	}
	public void addTable(String databaseName,String tableName, String[] attr) throws ParserConfigurationException, SAXException, IOException, TransformerException {
		
		 DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
	     DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
	     Document document = documentBuilder.parse(xmlToRead);
	     
	     Collection<Database> databases = new ArrayList<Database>();
	     databases.add(new Database());
	     Element toAdd=null;
	     NodeList base=document.getElementsByTagName("DataBase");
	     for(int i=0;i<base.getLength();i++) {
	    	 if(base.item(i).getNodeType()==Node.ELEMENT_NODE) {
	    		 Element data=(Element) base.item(i);
	    		 if(data.getAttribute("dataBaseName").equalsIgnoreCase(databaseName)) {
	    			 toAdd=data;
	    			 break;
	    		 }
	    	 } 
	     }
	     XMLReader reader=new XMLReader(xmlToRead);
         Element port = document.createElement("Table");
         port.setTextContent("\n");
         port.setAttribute("fileName", reader.lastIndex()+"");
         port.setAttribute("rowLength", "500");
         port.setAttribute("tableName", tableName);
         
         Element structure=document.createElement("Structure");
         for(int i=0;attr[i]!=null;i++) {
        	 Element attribute=document.createElement("Attribute");
        	 attribute.setAttribute( "attributeName", attr[i]);
        	 i++;
        	 attribute.setAttribute("type", attr[i]);
         	 if(attr[i].equalsIgnoreCase("char")) {
         		i++;
         	 	attribute.setAttribute("length", attr[i]);
         	 }
         	 structure.appendChild(attribute);
         }
         port.appendChild(structure);
        
         Element pk=document.createElement("primaryKey");
         Element pkattr=document.createElement("pkAttribute");
         pkattr.setTextContent(attr[0]);
         pk.appendChild(pkattr);
         Element indexfile=document.createElement("IndexFiles");
         indexfile.setTextContent("\n");
         Element fk=document.createElement("ForeignKeys");
         fk.setTextContent("\n");
         port.appendChild(indexfile);
         port.appendChild(fk);
         port.appendChild(pk);

         toAdd.appendChild(port); 
		
		 DOMSource source = new DOMSource(document);
		 TransformerFactory transformerFactory = TransformerFactory.newInstance();
		 Transformer transformer = transformerFactory.newTransformer();
		 StreamResult result = new StreamResult(xmlToRead);
		 transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		 transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
		 transformer.transform(source, result);
		 index++;
		
	}

	public void addIndexFile(String databaseName, String tableName, String attributeName) throws SAXException, IOException, ParserConfigurationException, TransformerException {
		 DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
	     DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
	     Document document = documentBuilder.parse(xmlToRead);
	     
	     Collection<Database> databases = new ArrayList<Database>();
	     databases.add(new Database());
	     Element toAddDB=null;
	     NodeList base=document.getElementsByTagName("DataBase");
	     for(int i=0;i<base.getLength();i++) {
	    	 if(base.item(i).getNodeType()==Node.ELEMENT_NODE) {
	    		 Element data=(Element) base.item(i);
	    		 if(data.getAttribute("dataBaseName").equalsIgnoreCase(databaseName)) {
	    			 toAddDB=data;
	    			 break;
	    		 }
	    	 } 
	     }
	     NodeList tables=toAddDB.getElementsByTagName("Table");
	     Element table=null;
	     for(int i=0;i<tables.getLength();i++) {
	    	 if(tables.item(i).getNodeType()==Node.ELEMENT_NODE) {
	    		 Element data=(Element) tables.item(i);
	    		 if(data.getAttribute("tableName").equalsIgnoreCase(tableName)) {
	    			 table=data;
	    			 break;
	    		 }
	    	 } 
	     }
	     
	     NodeList indexs=table.getElementsByTagName("IndexFiles");
	     Element indexContainer=(Element)indexs.item(0);
	     
	     XMLReader reader=new XMLReader(xmlToRead);
	     Element indexes=document.createElement("IndexFile");
	     indexes.setAttribute("fileName", reader.lastIndex()+"");
	     indexes.setAttribute("isUnique", "0");
	     indexes.setTextContent(attributeName);
	     indexContainer.appendChild(indexes);
		
		 DOMSource source = new DOMSource(document);
		 TransformerFactory transformerFactory = TransformerFactory.newInstance();
		 Transformer transformer = transformerFactory.newTransformer();
		 StreamResult result = new StreamResult(xmlToRead);
		 transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		 transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
		 transformer.transform(source, result);
	}
	
	public void addForeignKey(String databaseName, String tableName, String attributeName, String refferencedTable, String refferencedAttribute) throws DOMException, SAXException, IOException, ParserConfigurationException, TransformerException {
		 DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
	     DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
	     Document document = documentBuilder.parse(xmlToRead);
	     
	     Collection<Database> databases = new ArrayList<Database>();
	     databases.add(new Database());
	     Element toAddDB=null;
	     NodeList base=document.getElementsByTagName("DataBase");
	     for(int i=0;i<base.getLength();i++) {
	    	 if(base.item(i).getNodeType()==Node.ELEMENT_NODE) {
	    		 Element data=(Element) base.item(i);
	    		 if(data.getAttribute("dataBaseName").equalsIgnoreCase(databaseName)) {
	    			 toAddDB=data;
	    			 break;
	    		 }
	    	 } 
	     }
	     NodeList tables=toAddDB.getElementsByTagName("Table");
	     Element table=null;
	     for(int i=0;i<tables.getLength();i++) {
	    	 if(tables.item(i).getNodeType()==Node.ELEMENT_NODE) {
	    		 Element data=(Element) tables.item(i);
	    		 if(data.getAttribute("tableName").equalsIgnoreCase(tableName)) {
	    			 table=data;
	    			 break;
	    		 }
	    	 } 
	     }
	     
	     NodeList keys=table.getElementsByTagName("ForeignKeys");
	     Element keyContainer=(Element)keys.item(0);
	     
	     XMLReader reader=new XMLReader(xmlToRead);
	     Element foreign=document.createElement("ForeignKey");
	     foreign.setAttribute("fileName", reader.lastIndex()+"");
	     Element keyAtt = document.createElement("fkAttribute");
	     keyAtt.setTextContent(attributeName);
	     foreign.appendChild(keyAtt);
	     Element ref = document.createElement("references");
	     Element refTable = document.createElement("refTable");
	     refTable.setTextContent(refferencedTable);
	     ref.appendChild(refTable);
	     Element refAtt = document.createElement("refAttribute");
	     refAtt.setTextContent(refferencedAttribute);
	     ref.appendChild(refAtt);
	     foreign.appendChild(ref);
	     
	     keyContainer.appendChild(foreign);
		
		 DOMSource source = new DOMSource(document);
		 TransformerFactory transformerFactory = TransformerFactory.newInstance();
		 Transformer transformer = transformerFactory.newTransformer();
		 StreamResult result = new StreamResult(xmlToRead);
		 transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		 transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
		 transformer.transform(source, result);
	}
	
	 public static class Database {
	    public String getName() { return "foo"; }
	 }

	public String addUnique(String databaseName, String tableName, String attributeName) throws DOMException, SAXException, IOException, ParserConfigurationException, TransformerException {
		String giveBack="";
		
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
	     DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
	     Document document = documentBuilder.parse(xmlToRead);
	     
	     Collection<Database> databases = new ArrayList<Database>();
	     databases.add(new Database());
	     Element toAddDB=null;
	     NodeList base=document.getElementsByTagName("DataBase");
	     for(int i=0;i<base.getLength();i++) {
	    	 if(base.item(i).getNodeType()==Node.ELEMENT_NODE) {
	    		 Element data=(Element) base.item(i);
	    		 if(data.getAttribute("dataBaseName").equalsIgnoreCase(databaseName)) {
	    			 toAddDB=data;
	    			 break;
	    		 }
	    	 } 
	     }
	     NodeList tables=toAddDB.getElementsByTagName("Table");
	     Element table=null;
	     for(int i=0;i<tables.getLength();i++) {
	    	 if(tables.item(i).getNodeType()==Node.ELEMENT_NODE) {
	    		 Element data=(Element) tables.item(i);
	    		 if(data.getAttribute("tableName").equalsIgnoreCase(tableName)) {
	    			 table=data;
	    			 break;
	    		 }
	    	 } 
	     }
	     
	     NodeList indexs=table.getElementsByTagName("IndexFiles");
	     Element indexContainer=(Element)indexs.item(0);
	     
	     XMLReader reader=new XMLReader(xmlToRead);
	     Element indexes=document.createElement("IndexFile");
	     indexes.setAttribute("fileName", reader.lastIndex()+"");
	     indexes.setAttribute("isUnique", "1");
	     indexes.setTextContent(attributeName);
	     indexContainer.appendChild(indexes);
		
		 DOMSource source = new DOMSource(document);
		 TransformerFactory transformerFactory = TransformerFactory.newInstance();
		 Transformer transformer = transformerFactory.newTransformer();
		 StreamResult result = new StreamResult(xmlToRead);
		 transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		 transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
		 transformer.transform(source, result);
		
		return giveBack;
	}
	
}
