import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import redis.clients.jedis.Jedis;

public class RedisAcces {
	String xmlToRead;
	public RedisAcces(String xmlToRead) {
		this.xmlToRead=xmlToRead;
	}
	
	public boolean insert(String databaseName,String tableName, String key, String value) throws SAXException, IOException, ParserConfigurationException {
		Jedis jedis=new Jedis("localhost");
		XMLReader read=new XMLReader(xmlToRead);
		String unique=null;
		String foreign=null;
		String attributes=null;
		String indexes=null;
		attributes=read.readAllAtributeOnlyOfTable(databaseName, tableName);
		unique=read.readAllAttributeWithUniqueOfTable(databaseName, tableName);
		foreign=read.readAllForeignKeysOfTable(databaseName, tableName);
		indexes=read.readAllAttributeWithIndexOfTable(databaseName, tableName);
		
		String[] values=value.split("#");
		String[] att=attributes.split("#");
		String[] uniq=unique.split("#");
		String[] fore=foreign.split("#");
		String[] index=indexes.split("#");
		
		if(unique!="") {
			//Unique megszorit�s vizsg�lata majd besz�r�s a megfelel� indexfile-ba
			for(int i=1;i<att.length;i++)
				for(int j=0;j<uniq.length;j++) {
					// System.out.println(att[i]+"--"+values[i-1]+"->"+uniq[j]);
					if(att[i].equalsIgnoreCase(uniq[j])) {
						jedis.select(read.IndexOf(databaseName,tableName,uniq[j]));
						if(jedis.get(values[i-1])!=null) return false;
						else {
							//jedis.set(values[i-1], key);
							jedis.save();
						}
					}
				}
		}
		
		if(indexes!="") {
			//Nem unique indexekbe val� besz�r�s
			for(int i=1;i<att.length;i++)
				for(int j=0;j<index.length;j++) {
					//System.out.println(i+"->"+att[i]+"--"+values[i-1]+"->"+index[j]);
					//System.out.println(read.IndexOf(databaseName,tableName,index[j]));
					jedis.select(read.IndexOf(databaseName,tableName,index[j]));
					if(att[i].equalsIgnoreCase(index[j])) {
							jedis.set(values[i-1]+"#"+key, key);
							jedis.save();
						}
				}
		}
		
		if(foreign!=""){
			//K�ls� kulcsos megszorit�svizsg�lat, majd ha az apat�bl�ban van ilyen akkor besz�r�s
			for(int i=1;i<att.length;i++)
				for(int j=0;j<fore.length;j++) {
					if(att[i].equalsIgnoreCase(read.readDestinationOfForeignKey(databaseName, tableName, fore[j]).split("#")[0])) {
						jedis.select(read.tableIndex(databaseName,read.readDestinationOfForeignKey(databaseName, tableName, fore[j]).split("#")[0]));
						if(jedis.get(values[i-1])!=null) return false;
						else {
							jedis.select(read.indexOfForeign(databaseName,tableName,values[i-1]));
							//jedis.set(values[i-1]+"#"+key, key);
							jedis.save();
						}
					}
				}
		}
		jedis.select(read.tableIndex(databaseName, tableName));
		if(jedis.get(key) !=null) {
			jedis.close();
			return false;
		}
		jedis.set(key, value);
		jedis.save();
		jedis.close();
		return true;
	}
	public void delete(String databaseName,String tableName) throws SAXException, IOException, ParserConfigurationException {
		Jedis jedis=new Jedis("localhost");
		XMLReader read=new XMLReader(xmlToRead);
		jedis.select(read.tableIndex(databaseName, tableName));
    	jedis.flushDB();
    	
    	jedis.save();
    	jedis.close();
	}
	public boolean deletePrimaryKey(String databaseName,String tableName,String key) throws SAXException, IOException, ParserConfigurationException {
		Jedis jedis=new Jedis("localhost");
		XMLReader read=new XMLReader(xmlToRead);
		jedis.select(read.tableIndex(databaseName, tableName));
		if(jedis.get(key) ==null) {
			jedis.close();
			return false;
		}else {
			jedis.del(key);
	    	jedis.save();
	    	jedis.close();
	    	return true;
		}
	}
	
	void matrixPrint(String[] fields, String[][] mat) {
		for(int i=0;i<fields.length;i++)
			System.out.print(fields[i]+"|");
		System.out.println("");
		for(int i=0;i<mat.length;i++) {
			for(int j=0;j<mat[i].length;j++)
				System.out.print(mat[i][j]+"|");
			System.out.println("");
		}
		System.out.println("");
	}
	
	public String join(String algorithm, String databaseName, String toJoin, String where) throws SAXException, IOException, ParserConfigurationException {
		String giveBack="";
		Jedis jedis=new Jedis("localhost");
		XMLReader read=new XMLReader(xmlToRead);
		String[][] Joined = null;
		String [] joins = toJoin.split("#");
		int index=-1;
		String ATablaNev = joins[++index];
		String AJoinAttributum = joins[++index];
		String BTablaNev = joins[++index];
		String BJoinAttributum = joins[++index];
		String[] partsA=null;
		String[] partsB=null;
		if(algorithm.equals("1")) {
			//indexd nested loop join
			
			jedis.select(read.tableIndex(databaseName, ATablaNev));
			Set<String> namesA = jedis.keys("*");
			java.util.Iterator<String> itA = namesA.iterator();
	        String tmpA=read.readAllAtributeOnlyOfTable(databaseName, ATablaNev);
			partsA=tmpA.split("#");
			String[][] A=new String[namesA.size()][partsA.length];
			 int yA=-1;
		     int xA=-1;
		     while(itA.hasNext()) {
		    	 xA++;
		    	 yA=-1;
		    	 String s = itA.next();
		    	 yA++;
		    	 A[xA][yA]=s;
		    	 String[] tmpParts=jedis.get(s).split("#");
		    	 for(int i=0;i<tmpParts.length;i++)
		    		 A[xA][++yA]=tmpParts[i];
		     }
		    int whichA=0;
		    for(int i=0;i<partsA.length;i++)
	    		if(!partsA[i].equals(AJoinAttributum)) whichA++;
	    		else break;
		    matrixPrint(tmpA.split("#"),A);
		    jedis.select(read.tableIndex(databaseName, BTablaNev));
			Set<String> namesB = jedis.keys("*");
			java.util.Iterator<String> itB = namesB.iterator();
	        String tmpB=read.readAllAtributeOnlyOfTable(databaseName, BTablaNev);
			partsB=tmpB.split("#");
			String[][] B=new String[namesB.size()][partsB.length];
			 int yB=-1;
		     int xB=-1;
		     while(itB.hasNext()) {
		    	 xB++;
		    	 yB=-1;
		    	 String s = itB.next();
		    	 yB++;
		    	 B[xB][yB]=s;
		    	 String[] tmpParts=jedis.get(s).split("#");
		    	 for(int i=0;i<tmpParts.length;i++)
		    		 B[xB][++yB]=tmpParts[i];
		     }
		     int whichB=0;
			    for(int i=0;i<partsB.length;i++)
		    		if(!partsB[i].equals(BJoinAttributum)) whichB++;
		    		else break;
		    matrixPrint(tmpB.split("#"),B);
		    int indexJoined=-1;
		    int Bpointer=0;
		    Joined = new String[15][A[0].length+B[0].length];
		    for(int i=0;i<15;i++) {
		    	indexJoined=-1;
		    	for(int j=0;j<A[0].length;j++) {
		    		Joined[i][++indexJoined]=A[Bpointer][j];
		    	}
		    	int k=0;
		    	for(int j=0;j<B.length;j++) {
		    		if(Integer.parseInt(A[Bpointer][whichA])==Integer.parseInt(B[j][whichB])) { k=j; break; }
		    	}
		    	if(k!=0 && Bpointer<A.length-1) Bpointer++;
		    	//System.out.println(k);
		    	for(int j=0;j<B[0].length;j++) {
		    		Joined[i][++indexJoined]=B[k][j];
		    	}
		    }
		   // matrixPrint(tmpB.split("#"),Joined);
		    giveBack+=Bpointer+1+"#";
			giveBack+=Joined[0].length+"#";
			 for(int i=0;i<Bpointer+1;i++) {
				 for(int j=0;j<Joined[i].length;j++)
					giveBack+=Joined[i][j]+"#";
			}
		}else if(algorithm.equals("0")) {
			//Sort merge join
			//Lek�rek mindent A-b�l
			jedis.select(read.tableIndex(databaseName, ATablaNev));
			Set<String> namesA = jedis.keys("*");
			
	        java.util.Iterator<String> itA = namesA.iterator();
	        String tmpA=read.readAllAtributeOnlyOfTable(databaseName, ATablaNev);
			partsA=tmpA.split("#");
			String[][] A=new String[namesA.size()][partsA.length];
			 int yA=-1;
		     int xA=-1;
		     while(itA.hasNext()) {
		    	 xA++;
		    	 yA=-1;
		    	 String s = itA.next();
		    	 yA++;
		    	 A[xA][yA]=s;
		    	 String[] tmpParts=jedis.get(s).split("#");
		    	 for(int i=0;i<tmpParts.length;i++)
		    		 A[xA][++yA]=tmpParts[i];
		     }
		   // matrixPrint(tmpA.split("#"),A);
		    
		    //Rendezz�k A-t a join attributum szerint
		    int whichA=0;
	    	for(int i=0;i<partsA.length;i++)
	    		if(!partsA[i].equals(AJoinAttributum)) whichA++;
	    		else break;
	    	final int toSortA = whichA;
	    	Arrays.sort(A, new Comparator<String[]>() {
				@Override
				public int compare(String[] o1, String[] o2) {
					String item1 = o1[toSortA];
					String item2 = o2[toSortA];
					return item1.compareTo(item2);
				}
			});
		    //matrixPrint(tmpA.split("#"),A);
		    //Lek�rek mindent B-b�l
		    jedis.select(read.tableIndex(databaseName, BTablaNev));
			Set<String> namesB = jedis.keys("*");
	        java.util.Iterator<String> itB = namesB.iterator();
	        String tmpB=read.readAllAtributeOnlyOfTable(databaseName, BTablaNev);
			partsB=tmpB.split("#");
			String[][] B=new String[namesB.size()][partsB.length];
			 int yB=-1;
		     int xB=-1;
		     while(itB.hasNext()) {
		    	 xB++;
		    	 yB=-1;
		    	 String s = itB.next();
		    	 yB++;
		    	 B[xB][yB]=s;
		    	 String[] tmpParts=jedis.get(s).split("#");
		    	 for(int i=0;i<tmpParts.length;i++)
		    		 B[xB][++yB]=tmpParts[i];
		     }
		    //matrixPrint(tmpB.split("#"),B);
		    
		    //Rendezz�k B-t a join attributum szerint
		    int whichB=0;
	    	for(int i=0;i<partsB.length;i++)
	    		if(!partsB[i].equals(BJoinAttributum)) whichB++;
	    		else break;
	    	final int toSortB = whichB;
	    	Arrays.sort(B, new Comparator<String[]>() {
				@Override
				public int compare(String[] o1, String[] o2) {
					String item1 = o1[toSortB];
					String item2 = o2[toSortB];
					return item1.compareTo(item2);
				}
			});
		    //matrixPrint(tmpB.split("#"),B);
		    if(A.length>B.length) Joined = new String[15][A[0].length+B[0].length];
		    else Joined = new String[15][A[0].length+B[0].length];
		    int db=0;
		    //Konkr�t join algoritmuska!
		    if(A.length>B.length) {
			    int Apointer = 0;
			    int Bpointer = 0;
			    int nagyobb = 0;
			    int indexJoined=0;
			    for(int i=0;i<A.length;i++) {
			    	indexJoined=0;
			    	for(int j=0;j<A[0].length;j++) {
			    		Joined[i][++indexJoined]=A[Apointer][j];
			    	}
			    	for(int j=0;j<B[0].length;j++) {
			    		if(j!=whichB) Joined[i][++indexJoined]=B[Bpointer][j];
			    	}
			    	if(Apointer<Bpointer) Apointer++;
		    		else Bpointer++;
			    }
			   // matrixPrint(null,Joined);
		    }else {
		    	int Apointer = 0;
			    int Bpointer = 0;
			    int nagyobb = 0;
			    int indexJoined=-1;
			    //String[][] Joined = new String[B.length][namesB.size()+namesA.size()-1];
			    for(int i=0;i<15;i++) {
			    	indexJoined=-1;
			    	for(int j=0;j<B[0].length;j++) {
			    		Joined[i][++indexJoined]=B[Bpointer][j];
			    	}
			    	//matrixPrint(tmpA.split("#"),A);
			    	
			    		//System.out.println(Apointer+"--" +A[0].length+"--"+j+"--"+Joined[0].length);
			    		if(A.length>Apointer && Integer.parseInt(A[Apointer][whichA])==Integer.parseInt(B[Bpointer][whichB])) {
			    			for(int j=0;j<A[0].length;j++) {
			    				Joined[i][++indexJoined]=A[Apointer][j];
			    			}
			    			db++;
			    			if(Apointer<A.length-1) Apointer++;
			    			else Bpointer++;
			    			//if(Integer.parseInt(A[Apointer][whichA])!=Integer.parseInt(B[Bpointer][whichB])) Bpointer++;
			    		}

			    	if(Apointer<A.length-1) {
				    	if(Integer.parseInt(A[Apointer][whichA])<Integer.parseInt(B[Bpointer][whichB])) {db++;Apointer++; }
			    		else if(Integer.parseInt(A[Apointer][whichA])>Integer.parseInt(B[Bpointer][whichB])) {Bpointer++;db++;}
			    		//else if(Integer.parseInt(A[Apointer][whichA])==Integer.parseInt(B[Bpointer][whichB])) Apointer++;
			    	} else if(Integer.parseInt(A[Apointer][whichA])>Integer.parseInt(B[Bpointer][whichB])) {Bpointer++;db++;}
			    }
			   // matrixPrint(tmpA.split("#"),Joined);
		    }
		    System.out.println(db);
			 giveBack+=db+"#";
			 giveBack+=Joined[0].length+"#";
			 for(int i=0;i<db;i++) {
				 for(int j=0;j<Joined[i].length;j++)
					giveBack+=Joined[i][j]+"#";
			}
			
		}
		if(where!="") {
			giveBack="";
			
			String[] cond=where.split("#");
		     index=0;
		     while(index<cond.length) {
		    	 int which=0;
		    	 String[] parts = null;
		    	 if(cond[0+index].equals(ATablaNev)) parts = partsA;
		    	 if(cond[0+index].equals(BTablaNev)) parts = partsB;
		    	 for(int i=0;i<parts.length;i++)
		    		 if(!parts[i].equals(cond[1+index]))
		    			 which++;
		    		 else break;
		    	 //System.out.println(which);
		    	 switch(cond[2+index]) {
		    	 	case "==":{
		    	 		int ind[] = new int[Joined.length];
		    	 		int k=-1;
		    	 		for(int i=0;i<Joined.length;i++) {
		    	 			if(Joined[i][which].equals(cond[3+index]))
		    	 				ind[++k]=i;
		    	 		}
		    	 		String[][] tmpMat=new String[Joined.length][Joined[0].length];
		    	 		int tmpx=-1;;
		    	 		int tmpy=-1;;
		    	 		for(int j=0;j<k+1;j++) {
		    	 			tmpy=-1;
		    	 			tmpx++;
			    	 		for(int i=0;i<Joined[0].length;i++) 
			    	 			tmpMat[tmpx][++tmpy]=Joined[ind[j]][i];
		    	 		}
		    	 		Joined=Arrays.copyOf(tmpMat, k+1);
		    	 		break;
		    	 	}
		    	 	case "!=":{
		    	 		int ind[] = new int[Joined.length];
		    	 		int k=-1;
		    	 		for(int i=0;i<Joined.length;i++) {
		    	 			if(!Joined[i][which].equals(cond[3+index]))
		    	 				ind[++k]=i;
		    	 		}
		    	 		String[][] tmpMat=new String[Joined.length][Joined[0].length];
		    	 		int tmpx=-1;
		    	 		int tmpy=-1;
		    	 		for(int j=0;j<k+1;j++) {
		    	 			tmpy=-1;
		    	 			tmpx++;
			    	 		for(int i=0;i<Joined[0].length;i++) 
			    	 			tmpMat[tmpx][++tmpy]=Joined[ind[j]][i];
		    	 		}
		    	 		Joined=Arrays.copyOf(tmpMat, k+1);
		    	 		break;
		    	 	}
		    	 	case "<=":{
		    	 		int ind[] = new int[Joined.length];
		    	 		int k=-1;
		    	 		for(int i=0;i<Joined.length;i++) {
		    	 			if(Integer.parseInt(Joined[i][which]) <= Integer.parseInt(cond[3+index]))
		    	 				ind[++k]=i;
		    	 		}
		    	 		String[][] tmpMat=new String[Joined.length][Joined[0].length];
		    	 		int tmpx=-1;;
		    	 		int tmpy=-1;;
		    	 		for(int j=0;j<k+1;j++) {
		    	 			tmpy=-1;
		    	 			tmpx++;
			    	 		for(int i=0;i<Joined[0].length;i++) 
			    	 			tmpMat[tmpx][++tmpy]=Joined[ind[j]][i];
		    	 		}
		    	 		Joined=Arrays.copyOf(tmpMat, k+1);
		    	 		break;
		    	 	}
		    	 	case ">=":{
		    	 		int ind[] = new int[Joined.length];
		    	 		int k=-1;
		    	 		for(int i=0;i<Joined.length;i++) {
		    	 			if(Integer.parseInt(Joined[i][which]) >= Integer.parseInt(cond[3+index]))
		    	 				ind[++k]=i;
		    	 		}
		    	 		String[][] tmpMat=new String[Joined.length][Joined[0].length];
		    	 		int tmpx=-1;;
		    	 		int tmpy=-1;;
		    	 		for(int j=0;j<k+1;j++) {
		    	 			tmpy=-1;
		    	 			tmpx++;
			    	 		for(int i=0;i<Joined[0].length;i++) 
			    	 			tmpMat[tmpx][++tmpy]=Joined[ind[j]][i];
		    	 		}
		    	 		Joined=Arrays.copyOf(tmpMat, k+1);
		    	 		break;
		    	 	}
		    	 	case ">":{
		    	 		int ind[] = new int[Joined.length];
		    	 		int k=-1;
		    	 		for(int i=0;i<Joined.length;i++) {
		    	 			if(Integer.parseInt(Joined[i][which]) > Integer.parseInt(cond[3+index]))
		    	 				ind[++k]=i;
		    	 		}
		    	 		String[][] tmpMat=new String[Joined.length][Joined[0].length];
		    	 		int tmpx=-1;;
		    	 		int tmpy=-1;;
		    	 		for(int j=0;j<k+1;j++) {
		    	 			tmpy=-1;
		    	 			tmpx++;
			    	 		for(int i=0;i<Joined[0].length;i++) 
			    	 			tmpMat[tmpx][++tmpy]=Joined[ind[j]][i];
		    	 		}
		    	 		Joined=Arrays.copyOf(tmpMat, k+1);
		    	 		break;
		    	 	}
		    	 	case "<":{
		    	 		int ind[] = new int[Joined.length];
		    	 		int k=-1;
		    	 		for(int i=0;i<Joined.length;i++) {
		    	 			if(Integer.parseInt(Joined[i][which]) < Integer.parseInt(cond[3+index]))
		    	 				ind[++k]=i;
		    	 		}
		    	 		String[][] tmpMat=new String[Joined.length][Joined[0].length];
		    	 		int tmpx=-1;;
		    	 		int tmpy=-1;;
		    	 		for(int j=0;j<k+1;j++) {
		    	 			tmpy=-1;
		    	 			tmpx++;
			    	 		for(int i=0;i<Joined[0].length;i++) 
			    	 			tmpMat[tmpx][++tmpy]=Joined[ind[j]][i];
		    	 		}
		    	 		Joined=Arrays.copyOf(tmpMat, k+1);
		    	 		break;
		    	 	}
		    	 }
		    	 index+=5;
		    	 matrixPrint(parts,Joined); 
		     }
		     giveBack+=Joined.length+"#";
			 giveBack+=Joined[0].length+"#";
			 for(int i=0;i<Joined.length;i++) {
				 for(int j=0;j<Joined[i].length;j++)
					giveBack+=Joined[i][j]+"#";
			}
		}
		jedis.close();
		return giveBack;
	}
	
	public String select(String databaseName,String tableName, String where) throws SAXException, IOException, ParserConfigurationException {
		String giveBack="";
		Jedis jedis=new Jedis("localhost");
		XMLReader read=new XMLReader(xmlToRead);
		jedis.select(read.tableIndex(databaseName, tableName));
		
		Set<String> names = jedis.keys("*");
        java.util.Iterator<String> it = names.iterator();
        
        String tmp=read.readAllAtributeOnlyOfTable(databaseName, tableName);
		String[] parts=tmp.split("#");
		if(where!=null) System.out.println(where);
		String[][] mat=new String[names.size()][parts.length];
		if(where==null) {
	        giveBack+=names.size()+"#";
	        giveBack+=parts.length+"#";
	        while(it.hasNext()) {
	            String s = it.next();
	            giveBack +=s+"#" +jedis.get(s);
	        }
		}else {
		     int y=-1;
		     int x=-1;
		     while(it.hasNext()) {
		    	 x++;
		    	 y=-1;
		    	 String s = it.next();
		    	 y++;
		    	 mat[x][y]=s;
		    	 String[] tmpParts=jedis.get(s).split("#");
		    	 for(int i=0;i<tmpParts.length;i++)
		    		 mat[x][++y]=tmpParts[i];
		     }
		     String[] cond=where.split("#");
		     int index=0;
		     while(index<cond.length) {
		    	 int which=0;
		    	 //System.out.println(parts[0]+"-->"+cond[0]);
		    	 for(int i=0;i<parts.length;i++)
		    		 if(!parts[i].equals(cond[0+index]))
		    			 which++;
		    		 else break;
		    	 //System.out.println(which);
		    	 switch(cond[1+index]) {
		    	 	case "==":{
		    	 		int ind[] = new int[mat.length];
		    	 		int k=-1;
		    	 		for(int i=0;i<mat.length;i++) {
		    	 			if(mat[i][which].equals(cond[2+index]))
		    	 				ind[++k]=i;
		    	 		}
		    	 		String[][] tmpMat=new String[names.size()][parts.length];
		    	 		int tmpx=-1;;
		    	 		int tmpy=-1;;
		    	 		for(int j=0;j<k+1;j++) {
		    	 			tmpy=-1;
		    	 			tmpx++;
			    	 		for(int i=0;i<mat[0].length;i++) 
			    	 			tmpMat[tmpx][++tmpy]=mat[ind[j]][i];
		    	 		}
		    	 		mat=Arrays.copyOf(tmpMat, k+1);
		    	 		break;
		    	 	}
		    	 	case "!=":{
		    	 		int ind[] = new int[mat.length];
		    	 		int k=-1;
		    	 		for(int i=0;i<mat.length;i++) {
		    	 			if(!mat[i][which].equals(cond[2+index]))
		    	 				ind[++k]=i;
		    	 		}
		    	 		String[][] tmpMat=new String[names.size()][parts.length];
		    	 		int tmpx=-1;;
		    	 		int tmpy=-1;;
		    	 		for(int j=0;j<k+1;j++) {
		    	 			tmpy=-1;
		    	 			tmpx++;
			    	 		for(int i=0;i<mat[0].length;i++) 
			    	 			tmpMat[tmpx][++tmpy]=mat[ind[j]][i];
		    	 		}
		    	 		mat=Arrays.copyOf(tmpMat, k+1);
		    	 		break;
		    	 	}
		    	 	case "<=":{
		    	 		int ind[] = new int[mat.length];
		    	 		int k=-1;
		    	 		for(int i=0;i<mat.length;i++) {
		    	 			if(Integer.parseInt(mat[i][which]) <= Integer.parseInt(cond[2+index]))
		    	 				ind[++k]=i;
		    	 		}
		    	 		String[][] tmpMat=new String[names.size()][parts.length];
		    	 		int tmpx=-1;;
		    	 		int tmpy=-1;;
		    	 		for(int j=0;j<k+1;j++) {
		    	 			tmpy=-1;
		    	 			tmpx++;
			    	 		for(int i=0;i<mat[0].length;i++) 
			    	 			tmpMat[tmpx][++tmpy]=mat[ind[j]][i];
		    	 		}
		    	 		mat=Arrays.copyOf(tmpMat, k+1);
		    	 		break;
		    	 	}
		    	 	case ">=":{
		    	 		int ind[] = new int[mat.length];
		    	 		int k=-1;
		    	 		for(int i=0;i<mat.length;i++) {
		    	 			if(Integer.parseInt(mat[i][which]) >= Integer.parseInt(cond[2+index]))
		    	 				ind[++k]=i;
		    	 		}
		    	 		String[][] tmpMat=new String[names.size()][parts.length];
		    	 		int tmpx=-1;;
		    	 		int tmpy=-1;;
		    	 		for(int j=0;j<k+1;j++) {
		    	 			tmpy=-1;
		    	 			tmpx++;
			    	 		for(int i=0;i<mat[0].length;i++) 
			    	 			tmpMat[tmpx][++tmpy]=mat[ind[j]][i];
		    	 		}
		    	 		mat=Arrays.copyOf(tmpMat, k+1);
		    	 		break;
		    	 	}
		    	 	case ">":{
		    	 		int ind[] = new int[mat.length];
		    	 		int k=-1;
		    	 		for(int i=0;i<mat.length;i++) {
		    	 			if(Integer.parseInt(mat[i][which]) > Integer.parseInt(cond[2+index]))
		    	 				ind[++k]=i;
		    	 		}
		    	 		String[][] tmpMat=new String[names.size()][parts.length];
		    	 		int tmpx=-1;;
		    	 		int tmpy=-1;;
		    	 		for(int j=0;j<k+1;j++) {
		    	 			tmpy=-1;
		    	 			tmpx++;
			    	 		for(int i=0;i<mat[0].length;i++) 
			    	 			tmpMat[tmpx][++tmpy]=mat[ind[j]][i];
		    	 		}
		    	 		mat=Arrays.copyOf(tmpMat, k+1);
		    	 		break;
		    	 	}
		    	 	case "<":{
		    	 		int ind[] = new int[mat.length];
		    	 		int k=-1;
		    	 		for(int i=0;i<mat.length;i++) {
		    	 			if(Integer.parseInt(mat[i][which]) < Integer.parseInt(cond[2+index]))
		    	 				ind[++k]=i;
		    	 		}
		    	 		String[][] tmpMat=new String[names.size()][parts.length];
		    	 		int tmpx=-1;;
		    	 		int tmpy=-1;;
		    	 		for(int j=0;j<k+1;j++) {
		    	 			tmpy=-1;
		    	 			tmpx++;
			    	 		for(int i=0;i<mat[0].length;i++) 
			    	 			tmpMat[tmpx][++tmpy]=mat[ind[j]][i];
		    	 		}
		    	 		mat=Arrays.copyOf(tmpMat, k+1);
		    	 		break;
		    	 	}
		    	 }
		    	 index+=3;
		    	 matrixPrint(parts,mat); 
		     }
		     giveBack+=mat.length+"#";
			 giveBack+=mat[0].length+"#";
			 for(int i=0;i<mat.length;i++) {
				 for(int j=0;j<mat[i].length;j++)
					giveBack+=mat[i][j]+"#";
			}
		}
		
        jedis.close();
        return giveBack;
	}
	
	public void createIndex(String databaseName, String tableName, String attributeName) throws SAXException, IOException, ParserConfigurationException {
		Jedis jedis=new Jedis("localhost");
		XMLReader read=new XMLReader(xmlToRead);
		String indexes=read.readAllAttributeWithIndexOfTable(databaseName, tableName);
		String[] index=indexes.split("#");
		if(indexes!="") {
		//Nem unique indexekbe val� besz�r�s
			for(int j=0;j<index.length;j++) {
				if( attributeName.equalsIgnoreCase(index[j])) {
					jedis.select(read.tableIndex(databaseName, tableName));
					Set<String> names = jedis.keys("*");
			        java.util.Iterator<String> it = names.iterator();
			        while(it.hasNext()) {
			            String s = it.next();
			            String values =jedis.get(s);
			            String[] value = values.split("#");
			            jedis.select(read.IndexOf(databaseName,tableName,attributeName));
						jedis.set(value[j]+"#"+s, s);
						jedis.save();
			        }
				}
			}
		}
		
		jedis.save();
    	jedis.close();
	}
}


