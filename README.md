This project was established with Török Miklós Levente.
It is a relational database managament system server, which was
done in Java, the Client part was done by the person mentioned above in C#.
It uses Redis to store the data and XML to store the relations.
The usage is simple, there are 19 posible command to send to the server,
these looks like this: 
3#DatabaseName#TableName

So first the number of the command, and after thet the command specific
parameters divided by #.
This server can do the following: 
- 1 List all databases
- 2 List all table of a specific database
- 3 List the atribute of a specific table
- 4 Select all data from a table
- 5 Delete a database
- 6 Delete a table of specific database
- 7 Create a new database
- 8 Create a new table for an existing database
- 9 Insert data into an existing table
- 10 Delete all data from an existing table
- 11 Delete a record by given primary key 
- 12 Create a coustome index file 
- 13 Create a foreign key from an existing datacolumn
- 14 Read all unique atribute of a table
- 15 Read all foreign keys of a table
- 16 Read where the foreign keys belongs to
- 17 Add a new unique collumn to a table
- 18 Select by WHERE conditions  
- 19 Select by JOIN multiple tables